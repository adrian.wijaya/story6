from django.test import TestCase,Client, LiveServerTestCase
from django.urls import resolve
from .views import *
from .models import Status
from .form import StatusForm
from django.http import HttpRequest
from datetime import date, datetime, timedelta
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.color import Color
import unittest


# Create your tests here.

class Tests(TestCase):
    def test_home_page_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_add_status_is_exist(self):
        response = Client().get('/add-status/')
        self.assertEqual(response.status_code,302)   # Redirect

    def test_other_page_is_not_exist(self):
        response = Client().get('/update/')
        self.assertEqual(response.status_code,404)

    def test_index_page_is_work_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_index_page_using_index_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

    def test_indexpage_header(self):
        response = self.client.get('/')
        self.assertContains(response, 'Halo, Apa kabar ?')

    def test_status(self):
        Status.objects.create(status='Hello World!')
        response = self.client.get('/')
        self.assertContains(response, 'Hello World!')
        self.assertEqual(Status.objects.all().count(), 1)

    def test_add_status_is_work_func(self):
        found = resolve('/add-status/')
        self.assertEqual(found.func, add_status)

    def test_post_status(self):
        self.client.post('/add-status/', {
            'status': 'My Status'
        })
        response = self.client.get('/')
        self.assertContains(response, 'My Status')
        self.assertEqual(Status.objects.count(), 1)
        self.assertEqual(Status.objects.first().status, 'My Status')

    def test_no_data(self):
        form = StatusForm()
        self.assertFalse(form.is_valid())

    def test_valid_data(self):
        form = StatusForm({'status' : 'halo'})
        form = form.save()
        self.assertEqual(form.status, "halo")

    def test_invalid_data(self):
        form = StatusForm({ 'status' : 500 * ' ' })
        self.assertFalse(form.is_valid())

class Challenge(TestCase):
    def test_profile_page_is_exist(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code,200)

    def test_profile_page_content(self):
        response = self.client.get('/profile/')
        self.assertContains(response, 'Adrian Wijaya')

    def test_profile_page_is_work_func(self):
        found = resolve('/profile/')
        self.assertEqual(found.func, profile)

    def test_profile_page_has_picture(self):
        response = profile(HttpRequest())
        response = response.content.decode("utf8")
        self.assertIn("<img", response)

    def test_index_page_using_index_template(self):
        response = Client().get('/profile/')
        self.assertTemplateUsed(response, 'profile.html')

class FunctionalTest(TestCase):
    
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--disable-dev-shm-usage')
        chrome_options.add_argument("--disable-infobars")
        self.browser  = webdriver.Chrome('./chromedriver',chrome_options = chrome_options)
	

    def tearDown(self):
        self.browser.quit()

    def test_status(self):
        self.browser.get("http://story6-adrian.herokuapp.com/")
        status = self.browser.find_element_by_id('id_status')
        submit = self.browser.find_element_by_xpath('/html/body/div[1]/div[2]/div[1]/div[1]/form/button')
        status.send_keys('Coba Coba')
        submit.send_keys(Keys.RETURN)
        self.assertIn('Coba Coba', self.browser.page_source)


    '''
    Challenge Functional tests, 2 test for CSS property and
    other for layout property
    '''

    def test_css_property_back_color(self):
        self.browser.get("http://story6-adrian.herokuapp.com/")
        back_color = self.browser.find_element_by_tag_name('body').value_of_css_property('background-color')
        back_color = Color.from_string(back_color).hex
        self.assertEquals(back_color, '#ffffff')

    def test_css_property_button_border_color(self):
        self.browser.get("http://story6-adrian.herokuapp.com/")
        border_color = self.browser.find_element_by_tag_name('button').value_of_css_property('border-color')
        border_color = Color.from_string(border_color).hex
        self.assertEquals(border_color, '#28a745')

    def test_layout_navbar(self):
        self.browser.get("http://story6-adrian.herokuapp.com/")
        navbar = self.browser.find_element_by_tag_name('nav').text
        self.assertIn("Home", navbar)
        self.assertIn("My Profile", navbar)

    def test_layout_footer(self):
        self.browser.get("http://story6-adrian.herokuapp.com/")
        footer = self.browser.find_element_by_id('footer').text
        self.assertIn("@2019 Adrian Wijaya. All rights reserved.", footer)





     


