from django.db import models
from django.utils import timezone

# Create your models here.
class Status(models.Model):		
	status = models.CharField(max_length=300)
	time = models.TimeField(default=timezone.localtime(timezone.now()))
