# Generated by Django 2.1.1 on 2019-03-12 08:27

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Status',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('status', models.CharField(max_length=300)),
                ('time', models.TimeField(default=datetime.datetime(2019, 3, 12, 8, 27, 37, 583320, tzinfo=utc))),
            ],
        ),
    ]
