from django.shortcuts import render,HttpResponseRedirect
from django.utils import timezone
from django.contrib import messages
from django.urls import reverse
from .form import StatusForm
from .models import Status


def index(request):
	status_form = StatusForm()
	response = {
    	'date' : timezone.localtime(timezone.now()),
		'forms' : status_form,
		'status' : Status.objects.all(),
	}
	return render(request, 'index.html', response)

def add_status(request):
	status_form = StatusForm(request.POST)
	if (request.method == 'POST' and status_form.is_valid()) :
		status_form.save()
		status_form = StatusForm()
	return HttpResponseRedirect(reverse('home'))

def profile(request):
	return render(request, 'profile.html', {'date' : timezone.localtime(timezone.now())})	