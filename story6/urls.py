from django.urls import path
from .views import *
#url for app
urlpatterns = [
    path('', index, name="home"),
    path('add-status/', add_status, name='add_status'),
    path('profile/', profile, name='profile'),
]
